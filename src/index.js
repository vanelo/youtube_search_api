import _ from 'lodash';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';

const API_kEY = "AIzaSyBXK0M3GGfFY_tI941uVg0vr1_aLIBPNEo";


//Creste a new component
class App extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.videoSearch('flute');

    }
    videoSearch(term){
        YTSearch({key: API_kEY, term: term}, (videos) => {
            //this.setState({videos: videos}); es lo mismo que
            this.setState({
                videos:videos,
                selectedVideo: videos[0]
            });
        });
    }
    //lodash se encarga de que la funcion solo pueda ser llamada cada tanto, en este caso hasta cada 300ms
    render(){
        const videoSearch = _.debounce((term)=>{ this.videoSearch(term)}, 300);
        return (
            <div>
                <SearchBar onSearchTermChange={ videoSearch}/>
                <VideoDetail video={this.state.selectedVideo}/>
                <VideoList 
                    onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                    videos={this.state.videos}/>
            </div>
        );
    }
}
//Take this component's generated HTML 
ReactDOM.render(
    <App />,
    document.querySelector('.container')    
);